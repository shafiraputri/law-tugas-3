import logging
from fastapi import FastAPI
from logstash_async.handler import AsynchronousLogstashHandler
from logstash_async.handler import LogstashFormatter

app = FastAPI()

logger = logging.getLogger("logstash")
logger.setLevel(logging.DEBUG)

handler = AsynchronousLogstashHandler(
    host='bb1e9218-b904-4ae6-8bbb-6faf3237ce89-ls.logit.io',
    port=23271,
    ssl_enable=False,
    ssl_verify=False,
    database_path='')

formatter = LogstashFormatter()
handler.setFormatter(formatter)

logger.addHandler(handler)

@app.get("/")
def read_root():
    logger.debug('Root')
    return {"Hello": "World"}

@app.get("/echo")
def echo(param: str):
    extra = {
        "param": param
    }
    logger.debug('Echo Service - Param: ' + param, extra=extra)
    return {"param": param}

@app.get("/echo2")
def echo2(param1: str, param2: str):
    extra = {
        "param1": param1,
        "param2": param2,
    }
    logger.debug('Echo 2 Service - Param1: ' + param1 + ', Param2: ' + param2, extra=extra)
    return {"param1": param1, "param2": param2}
